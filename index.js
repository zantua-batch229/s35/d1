const express = require("express");
const mongoose = require("mongoose");

// Server preparation
const app = express();
const port = 3001;

// [SECTION] - MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.tmdxo2p.mongodb.net/B229_to-do?retryWrites=true&w=majority", {

	useNewUrlParser : true,
	useUnifiedTopology : true

});

// set notification for connection success or failure

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// [SECTION] - Schemas
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Task name is Required!"]
	},
	status: {
		type: String,
		default: "pending"
	}
});

// [SECTION] - Models
const Task = mongoose.model("Task", taskSchema);




const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Task username is Required!"]
	},
	password: {
		type: String,
		required: [true, "Task password is Required!"]
	},
	email: {type: String}
	
});
const User = mongoose.model("User", userSchema);




// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Creating a New Task
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/task", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		}else{
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New Task Created!");
				}
			})

		}

	})
})

// getting all tasks

app.get("/tasks", (req,res) => {
	Task.find({}, (err,result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	});
})

// add a user
app.post("/register", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send(`Username ${result.username} is taken.`);
		}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password,
				email: req.body.email
			});

			newUser.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New User Registered!");
				}
			})

		}

	})
})

// getting all users

app.get("/users", (req,res) => {
	User.find({}, (err,result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	});
})



app.listen(port, () => console.log(`Server running at port ${port}.`))

